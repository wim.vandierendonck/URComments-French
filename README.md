# URCom

This scripts are created to help editors that have at least reached the Level 2 to answer to URs more quickly

They are based on RickZabel's version.

The functions of this script are :
----------------------------------

- Complete the answer part with predefined texts based on the UR type
- Automaticaly send Reminders and close URs after some times (Defined in the settings by the editor, default 7 days to closure (Waze's  choise in the Wiki))
- Zoom on the click on URs and center the page on it
- Change the URs colors to more red color when they passed the limit age


The features :
--------------
- In the predefined texts are some variables used to change the text to the editor's signature
- a tab that have 3 childs tab
-- Comments : All the available predefined comments are there
-- UR Filter : A tab to filter the UR on some choises like the age, the last comment, the user's URs, ...
-- Settings : Some advanced settings like the signature, ...

Installation
------------

Install Tampermonkey/Greasemonkey depending on your browser.

Then use the following links to install.

<a href="https://greasyfork.org/fr/scripts/370223-wme-urcom">Main script (English)</a>

<a href="https://greasyfork.org/fr/scripts/370207-wme-urcom-francais-belgique">French translation</a> /!\ /!\ /!\ /!\ Main script required /!\ /!\ /!\ /!\

If you find any issue
---------------------
- press the report a bug on Tampermonkey/Greasemonkey (Prefered with follow)
- or send an email at <a href="mailto:incoming+WMEScripts/URComments-French@incoming.gitlab.com">incoming+WMEScripts/URComments-French@incoming.gitlab.com</a>
- or send me a DM on Slack Benelux (@tunisiano187)
- or send me a DM on Discord (@tunisiano187)

Todos/Issues
------------

- See <a href="https://gitlab.com/WMEScripts/URComments-French/issues">this page</a>